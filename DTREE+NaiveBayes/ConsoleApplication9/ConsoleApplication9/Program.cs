﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication9
{
    class Program
    {
        static void Main(string[] args)
        {
            {
               
                    System.IO.StreamReader file =
                           new System.IO.StreamReader("C:\\Users\\SHUBHANKAR\\Desktop\\New folder (2)\\data.csv");

                    int[] arr = new int[283];
                    string[] att = new string[8];
                    string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                    double total = 0.0, p_sex_0_y = 0.0, p_sex_1_y = 0.0, p_cp_1_y = 0.0, p_cp_2_y = 0.0, p_cp_3_y = 0.0, p_cp_4_y = 0.0, p_exang_0_y = 0.0, p_exang_1_y = 0.0, p_oldpeak_0_y = 0.0, p_oldpeak_1_y = 0.0,
                    p_slope_1_y = 0.0, p_slope_2_y = 0.0, p_slope_3_y = 0.0, p_ca_0_y = 0.0, p_ca_1_y = 0.0, p_ca_2_y = 0.0, p_ca_3_y = 0.0, p_thal_3_y = 0.0, p_thal_6_y = 0.0, p_thal_7_y = 0.0, p_num_0_y = 0.0, p_num_1_y = 0.0, p_heart_dis = 0.0, p_no_heart_dis = 0.0;

                    double p_sex_0_n = 0.0, p_sex_1_n = 0.0, p_cp_1_n = 0.0, p_cp_2_n = 0.0, p_cp_3_n = 0.0, p_cp_4_n = 0.0, p_exang_0_n = 0.0, p_exang_1_n = 0.0, p_oldpeak_0_n = 0.0, p_oldpeak_1_n = 0.0,
                    p_slope_1_n = 0.0, p_slope_2_n = 0.0, p_slope_3_n = 0.0, p_ca_0_n = 0.0, p_ca_1_n = 0.0, p_ca_2_n = 0.0, p_ca_3_n = 0.0, p_thal_3_n = 0.0, p_thal_6_n = 0.0, p_thal_7_n = 0.0, p_num_0_n = 0.0, p_num_1_n = 0.0;


                    string pre = " ";

                    String line;
                    int nz = 0, no = 0;
                    while ((line = file.ReadLine()) != null)
                    {
                        ++total;
                        att = line.Split(',');
                        if (att[Array.IndexOf(attname, "sex")] == "0" && att[Array.IndexOf(attname, "num")] == "1")
                        {
                            ++p_sex_0_y;
                        }
                        else if (att[Array.IndexOf(attname, "sex")] == "1" && att[Array.IndexOf(attname, "num")] == "1")
                        {
                            ++p_sex_1_y;
                        }

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "cp")] == "1" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_cp_1_y;
                        else if (att[Array.IndexOf(attname, "cp")] == "2" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_cp_2_y;
                        else if (att[Array.IndexOf(attname, "cp")] == "3" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_cp_3_y;
                        else if (att[Array.IndexOf(attname, "cp")] == "4" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_cp_4_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "exang")] == "0" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_exang_0_y;
                        else if (att[Array.IndexOf(attname, "exang")] == "1" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_exang_1_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "oldpeak")] == "0" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_oldpeak_0_y;
                        else if (att[Array.IndexOf(attname, "oldpeak")] == "1" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_oldpeak_1_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "slope")] == "1" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_slope_1_y;
                        else if (att[Array.IndexOf(attname, "slope")] == "2" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_slope_2_y;
                        else if (att[Array.IndexOf(attname, "slope")] == "3" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_slope_3_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "ca")] == "0" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_ca_0_y;
                        else if (att[Array.IndexOf(attname, "ca")] == "1" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_ca_1_y;
                        else if (att[Array.IndexOf(attname, "ca")] == "2" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_ca_2_y;
                        else if (att[Array.IndexOf(attname, "ca")] == "3" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_ca_3_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "thal")] == "3" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_thal_3_y;
                        else if (att[Array.IndexOf(attname, "thal")] == "6" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_thal_6_y;
                        else if (att[Array.IndexOf(attname, "thal")] == "7" && att[Array.IndexOf(attname, "num")] == "1")
                            ++p_thal_7_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };


                        if (att[Array.IndexOf(attname, "num")] == "1")
                            p_heart_dis++;
                        else if (att[Array.IndexOf(attname, "num")] == "0")
                            p_no_heart_dis++;


                        //probability when no heart disease


                        if (att[Array.IndexOf(attname, "sex")] == "0" && att[Array.IndexOf(attname, "num")] == "0")
                        {
                            ++p_sex_0_n;
                        }
                        else if (att[Array.IndexOf(attname, "sex")] == "1" && att[Array.IndexOf(attname, "num")] == "0")
                        {
                            ++p_sex_1_n;
                        }

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "cp")] == "1" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_cp_1_n;
                        else if (att[Array.IndexOf(attname, "cp")] == "2" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_cp_2_n;
                        else if (att[Array.IndexOf(attname, "cp")] == "3" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_cp_3_n;
                        else if (att[Array.IndexOf(attname, "cp")] == "4" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_cp_4_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "exang")] == "0" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_exang_0_n;
                        else if (att[Array.IndexOf(attname, "exang")] == "1" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_exang_1_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "oldpeak")] == "0" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_oldpeak_0_n;
                        else if (att[Array.IndexOf(attname, "oldpeak")] == "1" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_oldpeak_1_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "slope")] == "1" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_slope_1_n;
                        else if (att[Array.IndexOf(attname, "slope")] == "2" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_slope_2_n;
                        else if (att[Array.IndexOf(attname, "slope")] == "3" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_slope_3_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "ca")] == "0" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_ca_0_n;
                        else if (att[Array.IndexOf(attname, "ca")] == "1" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_ca_1_n;
                        else if (att[Array.IndexOf(attname, "ca")] == "2" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_ca_2_n;
                        else if (att[Array.IndexOf(attname, "ca")] == "3" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_ca_3_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att[Array.IndexOf(attname, "thal")] == "3" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_thal_3_n;
                        else if (att[Array.IndexOf(attname, "thal")] == "6" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_thal_6_n;
                        else if (att[Array.IndexOf(attname, "thal")] == "7" && att[Array.IndexOf(attname, "num")] == "0")
                            ++p_thal_7_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };


                    }
                    
                    Console.WriteLine(p_sex_0_n + "=sex_0_n,\n" +
                    p_sex_1_n + "=sex_1_n,\n" +
                    p_cp_1_n + "=cp_1_n,\n" +
                    p_cp_2_n + "=cp_2_n,\n" +
                    p_cp_3_n + "=cp_3_n,\n" +
                    p_cp_4_n + "=cp_4_n,\n" +
                    p_exang_0_n + "=exang_0_n,\n" +
                    p_exang_1_n + "=exang_1_n,\n" +
                    p_oldpeak_0_n + "=oldpeak_0_n,\n" +
                    p_oldpeak_1_n + "=oldpeak_1_n,\n" +
                    p_slope_1_n + "=slope_1_n,\n" +
                    p_slope_2_n + "=slope_2_n,\n" +
                    p_slope_3_n + "=slope_3_n,\n" +
                    p_ca_0_n + "=ca_0_n,\n" +
                    p_ca_1_n + "=ca_1_n,\n" +
                    p_ca_2_n + "=ca_2_n,\n" +
                    p_ca_3_n + "=ca_3_n,\n" +
                    p_thal_3_n + "=thal_3_n,\n" +
                    p_thal_6_n + "=thal_6_n,\n" +
                    p_thal_7_n + "=thal_7_n,\n"+
                    p_heart_dis + "=num_0_y,\n" +
                    p_no_heart_dis + "=num_1__n,\n");

           
                   

                    p_sex_0_y = p_sex_0_y / p_heart_dis;
                    p_sex_1_y = p_sex_1_y / p_heart_dis;
                    p_cp_1_y = p_cp_1_y / p_heart_dis;
                    p_cp_2_y = p_cp_2_y / p_heart_dis;
                    p_cp_3_y = p_cp_3_y / p_heart_dis;
                    p_cp_4_y = p_cp_4_y / p_heart_dis;
                    p_exang_0_y = p_exang_0_y / p_heart_dis;
                    p_exang_1_y = p_exang_1_y / p_heart_dis;
                    p_oldpeak_0_y = p_oldpeak_0_y / p_heart_dis;
                    p_oldpeak_1_y = p_oldpeak_1_y / p_heart_dis;
                    p_slope_1_y = p_slope_1_y / p_heart_dis;
                    p_slope_2_y = p_slope_2_y / p_heart_dis;
                    p_slope_3_y = p_slope_3_y / p_heart_dis;
                    p_ca_0_y = p_ca_0_y / p_heart_dis;
                    p_ca_1_y = p_ca_1_y / p_heart_dis;
                    p_ca_2_y = p_ca_2_y / p_heart_dis;
                    p_ca_3_y = p_ca_2_y / p_heart_dis;
                    p_thal_3_y = p_thal_3_y / p_heart_dis;
                    p_thal_6_y = p_thal_6_y / p_heart_dis;
                    p_thal_7_y = p_thal_7_y / p_heart_dis;


                    

                    Console.WriteLine(p_sex_0_y + "=sex_0_y,\n" +
                    p_sex_1_y + "=sex_1_y,\n" +
                    p_cp_1_y + "=cp_1_y,\n" +
                    p_cp_2_y + "=cp_2_y,\n" +
                    p_cp_3_y + "=cp_3_y,\n" +
                    p_cp_4_y + "=cp_4_y,\n" +
                    p_exang_0_y + "=exang_0_y,\n" +
                    p_exang_1_y + "=exang_1_y,\n" +
                    p_oldpeak_0_y + "=oldpeak_0_y,\n" +
                    p_oldpeak_1_y + "=oldpeak1_y,\n" +
                    p_slope_1_y + "=slope_1_y,\n" +
                    p_slope_2_y + "=slope_2_y,\n" +
                    p_slope_3_y + "=slope_3_y,\n" +
                    p_ca_0_y + "=ca_0_y,\n" +
                    p_ca_1_y + "=ca_1_y,\n" +
                    p_ca_2_y + "=ca_2_y,\n" +
                    p_ca_3_y + "=ca_3_y,\n" +
                    p_thal_3_y + "=thal_3,\n" +
                    p_thal_6_y + "=thal_6,\n" +
                    p_thal_7_y + "=thal_7,\n" +
                    p_heart_dis + "=num_0_y,\n" +
                    p_no_heart_dis + "=num_1_y,\n");
                    
                    Console.WriteLine("prob for no heart disease");

                    p_sex_0_n = p_sex_0_n / p_no_heart_dis;
                    p_sex_1_n = p_sex_1_n / p_no_heart_dis;
                    p_cp_1_n = p_cp_1_n / p_no_heart_dis;
                    p_cp_2_n = p_cp_2_n / p_no_heart_dis;
                    p_cp_3_n = p_cp_3_n / p_no_heart_dis;
                    p_cp_4_n = p_cp_4_n / p_no_heart_dis;
                    p_exang_0_n = p_exang_0_n / p_no_heart_dis;
                    p_exang_1_n = p_exang_1_n / p_no_heart_dis;
                    p_oldpeak_0_n = p_oldpeak_0_n / p_no_heart_dis;
                    p_oldpeak_1_n = p_oldpeak_1_n / p_no_heart_dis;
                    p_slope_1_n = p_slope_1_n / p_no_heart_dis;
                    p_slope_2_n = p_slope_2_n / p_no_heart_dis;
                    p_slope_3_n = p_slope_3_n / p_no_heart_dis;
                    p_ca_0_n = p_ca_0_n / p_no_heart_dis;
                    p_ca_1_n = p_ca_1_n / p_no_heart_dis;
                    p_ca_2_n = p_ca_2_n / p_no_heart_dis;
                    p_ca_3_n = p_ca_2_n / p_no_heart_dis;
                    p_thal_3_n = p_thal_3_n / p_no_heart_dis;
                    p_thal_6_n = p_thal_6_n / p_no_heart_dis;
                    p_thal_7_n = p_thal_7_n / p_no_heart_dis;


                    
                    Console.WriteLine(p_sex_0_n + "=sex_0_n,\n" +
                    p_sex_1_n + "=sex_1_n,\n" +
                    p_cp_1_n + "=cp_1_n,\n" +
                    p_cp_2_n + "=cp_2_n,\n" +
                    p_cp_3_n + "=cp_3_n,\n" +
                    p_cp_4_n + "=cp_4_n,\n" +
                    p_exang_0_n + "=exang_0_n,\n" +
                    p_exang_1_n + "=exang_1_n,\n" +
                    p_oldpeak_0_n + "=oldpeak_0_n,\n" +
                    p_oldpeak_1_n + "=oldpeak_1_n,\n" +
                    p_slope_1_n + "=slope_1_n,\n" +
                    p_slope_2_n + "=slope_2_n,\n" +
                    p_slope_3_n + "=slope_3_n,\n" +
                    p_ca_0_n + "=ca_0_n,\n" +
                    p_ca_1_n + "=ca_1_n,\n" +
                    p_ca_2_n + "=ca_2_n,\n" +
                    p_ca_3_n + "=ca_3_n,\n" +
                    p_thal_3_n + "=thal_3_n,\n" +
                    p_thal_6_n + "=thal_6_n,\n" +
                    p_thal_7_n + "=thal_7_n,\n"
                    );
                    

                    p_no_heart_dis = p_no_heart_dis / total;
                    p_heart_dis = p_heart_dis / total;
                    // Console.ReadLine();
                    file.Close();

                    System.IO.StreamReader file1 =
                                   new System.IO.StreamReader("C:\\Users\\SHUBHANKAR\\Desktop\\New folder (2)\\test_data\\testingdata.csv");
                    System.IO.StreamWriter file2 =
                                   new System.IO.StreamWriter("C:\\Users\\SHUBHANKAR\\Desktop\\New folder (2)\\test_data\\predicted_data_naivebayes_DT_combined_temp.csv");

                    string[] att2 = new string[9];
                    att2[8] = " ";
                    double count = 0.0, count2 = 0.0;
                    double prody = 1.0, prodn = 1.0;

                    //for (int i = 0; i < 2;i++ )
                    while ((line = file1.ReadLine()) != null)
                    {  //  line = file1.ReadLine();
                        prody = 1.0;
                        prodn = 1.0;
                        att2 = line.Split(',');
                        if (att2[Array.IndexOf(attname, "sex")] == "0")
                        {
                            prody = prody * p_sex_0_y;
                        }
                        else if (att2[Array.IndexOf(attname, "sex")] == "1")
                        {
                            prody = prody * p_sex_1_y;
                        }

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "cp")] == "1")
                            prody = prody * p_cp_1_y;
                        else if (att2[Array.IndexOf(attname, "cp")] == "2")
                            prody = prody * p_cp_2_y;
                        else if (att2[Array.IndexOf(attname, "cp")] == "3")
                            prody = prody * p_cp_3_y;
                        else if (att2[Array.IndexOf(attname, "cp")] == "4")
                            prody = prody * p_cp_4_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "exang")] == "0")
                            prody = prody * p_exang_0_y;
                        else if (att2[Array.IndexOf(attname, "exang")] == "1")
                            prody = prody * p_exang_1_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "oldpeak")] == "0")
                            prody = prody * p_oldpeak_0_y;
                        else if (att2[Array.IndexOf(attname, "oldpeak")] == "1")
                            prody = prody * p_oldpeak_1_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "slope")] == "1")
                            prody = prody * p_slope_1_y;
                        else if (att2[Array.IndexOf(attname, "slope")] == "2")
                            prody = prody * p_slope_2_y;
                        else if (att2[Array.IndexOf(attname, "slope")] == "3")
                            prody = prody * p_slope_3_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "ca")] == "0")
                            prody = prody * p_ca_0_y;
                        else if (att2[Array.IndexOf(attname, "ca")] == "1")
                            prody = prody * p_ca_1_y;
                        else if (att2[Array.IndexOf(attname, "ca")] == "2")
                            prody = prody * p_ca_2_y;
                        else if (att2[Array.IndexOf(attname, "ca")] == "3")
                            prody = prody * p_ca_3_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "thal")] == "3")
                            prody = prody * p_thal_3_y;
                        else if (att2[Array.IndexOf(attname, "thal")] == "6")
                            prody = prody * p_thal_6_y;
                        else if (att2[Array.IndexOf(attname, "thal")] == "7")
                            prody = prody * p_thal_7_y;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };



                        //probability when no heart disease


                        if (att2[Array.IndexOf(attname, "sex")] == "0")
                        {
                            prodn = prodn * p_sex_0_n;
                        }
                        else if (att2[Array.IndexOf(attname, "sex")] == "1")
                        {
                            prodn = prodn * p_sex_1_n;
                        }

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "cp")] == "1")
                            prodn = prodn * p_cp_1_n;
                        else if (att2[Array.IndexOf(attname, "cp")] == "2")
                            prodn = prodn * p_cp_2_n;
                        else if (att2[Array.IndexOf(attname, "cp")] == "3")
                            prodn = prodn * p_cp_3_n;
                        else if (att2[Array.IndexOf(attname, "cp")] == "4")
                            prodn = prodn * p_cp_4_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "exang")] == "0")
                            prodn = prodn * p_exang_0_n;
                        else if (att2[Array.IndexOf(attname, "exang")] == "1")
                            prodn = prodn * p_exang_1_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "oldpeak")] == "0")
                            prodn = prodn * p_oldpeak_0_n;
                        else if (att2[Array.IndexOf(attname, "oldpeak")] == "1")
                            prodn = prodn * p_oldpeak_1_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "slope")] == "1")
                            prodn = prodn * p_slope_1_n;
                        else if (att2[Array.IndexOf(attname, "slope")] == "2")
                            prodn = prodn * p_slope_2_n;
                        else if (att2[Array.IndexOf(attname, "slope")] == "3")
                            prodn = prodn * p_slope_3_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "ca")] == "0")
                            prodn = prodn * p_ca_0_n;
                        else if (att2[Array.IndexOf(attname, "ca")] == "1")
                            prodn = prodn * p_ca_1_n;
                        else if (att2[Array.IndexOf(attname, "ca")] == "2")
                            prodn = prodn * p_ca_2_n;
                        else if (att2[Array.IndexOf(attname, "ca")] == "3")
                            prodn = prodn * p_ca_3_n;

                        //string[] attname = { "sex", "cp", "exang", "oldpeak", "slope", "ca", "thal", "num" };
                        if (att2[Array.IndexOf(attname, "thal")] == "3")
                            prodn = prodn * p_thal_3_n;
                        else if (att2[Array.IndexOf(attname, "thal")] == "6")
                            prodn = prodn * p_thal_6_n;
                        else if (att2[Array.IndexOf(attname, "thal")] == "7")
                            prodn = prodn * p_thal_7_n;



                        double tprody = prody * p_heart_dis;
                        double tprodn = prodn * p_no_heart_dis;


                        if (att2[Array.IndexOf(attname, "ca")] == "0")
                        {
                            if (att2[Array.IndexOf(attname, "oldpeak")] == "0")
                            {
                                if (att2[Array.IndexOf(attname, "thal")] == "3")
                                {
                                    if (att2[Array.IndexOf(attname, "exang")] == "0")
                                    {
                                        if (att2[Array.IndexOf(attname, "slope")] == "1")
                                        {
                                            if (att2[Array.IndexOf(attname, "sex")] == "0")
                                            {
                                                if (att2[Array.IndexOf(attname, "cp")] == "1" || att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3" || att2[Array.IndexOf(attname, "cp")] == "4")
                                                {
                                                    pre = "0";
                                                }

                                            }
                                            else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                            {
                                                if (att2[Array.IndexOf(attname, "cp")] == "1" || att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3" || att2[Array.IndexOf(attname, "cp")] == "4")
                                                {
                                                    pre = "0";
                                                }
                                            }
                                        }
                                        else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                        {
                                            if (att2[Array.IndexOf(attname, "sex")] == "0")
                                            {
                                                pre = "0";
                                            }
                                            else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                            {
                                                if (att2[Array.IndexOf(attname, "cp")] == "1" || att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3" || att2[Array.IndexOf(attname, "cp")] == "4")
                                                {
                                                    pre = "0";
                                                }
                                            }
                                        }
                                        else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                        {
                                            pre = "0";
                                        }


                                    }
                                    else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                    {
                                        if (att2[Array.IndexOf(attname, "sex")] == "0")
                                        {
                                            if (att2[Array.IndexOf(attname, "cp")] == "1" || att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3")
                                            {
                                                pre = "0";
                                            }
                                            else if (att2[Array.IndexOf(attname, "cp")] == "4")
                                            {
                                                if (att2[Array.IndexOf(attname, "slope")] == "1" || att2[Array.IndexOf(attname, "slope")] == "2" || att2[Array.IndexOf(attname, "slope")] == "3")
                                                {
                                                    pre = "0";
                                                }
                                            }
                                        }

                                        else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                        {
                                            if (att2[Array.IndexOf(attname, "cp")] == "1" || att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3" || att2[Array.IndexOf(attname, "cp")] == "0")
                                            {
                                                pre = "0";
                                            }

                                        }
                                    }


                                }
                                else if (att2[Array.IndexOf(attname, "thal")] == "6")
                                {
                                    if (att2[Array.IndexOf(attname, "exang")] == "0")
                                    {
                                        pre = "0";

                                    }
                                    else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                    {
                                        pre = "1";
                                    }
                                }
                                else if (att2[Array.IndexOf(attname, "thal")] == "7")
                                {
                                    if (att2[Array.IndexOf(attname, "cp")] == "1")
                                    {
                                        if (att2[Array.IndexOf(attname, "exang")] == "0")
                                            pre = "0";
                                        else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                            pre = "0";
                                    }
                                    else if (att2[Array.IndexOf(attname, "cp")] == "2")
                                    {
                                        if (att2[Array.IndexOf(attname, "slope")] == "1" || att2[Array.IndexOf(attname, "slope")] == "2")
                                            pre = "0";
                                        else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                            pre = "1";
                                    }
                                    else if (att2[Array.IndexOf(attname, "cp")] == "3")
                                    {
                                        pre = "0";
                                    }
                                    else if (att2[Array.IndexOf(attname, "cp")] == "4")
                                    {
                                        if (att2[Array.IndexOf(attname, "sex")] == "0")
                                            pre = "1";
                                        else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                        {
                                            if (att2[Array.IndexOf(attname, "slope")] == "1")
                                            {
                                                if (att2[Array.IndexOf(attname, "exang")] == "0")
                                                    pre = "1";
                                                else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                                    pre = "0";
                                            }
                                            else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                            {
                                                if (att2[Array.IndexOf(attname, "exang")] == "0")
                                                    pre = "1";
                                                else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                                    pre = "1";
                                            }
                                            else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                            {
                                                pre = "0";
                                            }
                                        }
                                    }
                                }

                            }
                            else if (att2[Array.IndexOf(attname, "oldpeak")] == "1")
                            {
                                if (att2[Array.IndexOf(attname, "thal")] == "3")
                                {
                                    if (att2[Array.IndexOf(attname, "slope")] == "1")
                                        pre = "0";
                                    else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                    {
                                        if (att2[Array.IndexOf(attname, "cp")] == "1")
                                            pre = "0";
                                        else if (att2[Array.IndexOf(attname, "cp")] == "2")
                                            pre = "1";
                                        else if (att2[Array.IndexOf(attname, "cp")] == "3")
                                        {
                                            if (att2[Array.IndexOf(attname, "sex")] == "0")
                                                pre = "0";
                                            else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                                pre = "0";
                                        }
                                        else if (att2[Array.IndexOf(attname, "cp")] == "4")
                                        {
                                            if (att2[Array.IndexOf(attname, "sex")] == "0")
                                            {
                                                if (att2[Array.IndexOf(attname, "exang")] == "0")
                                                    pre = "0";
                                                else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                                    pre = "0";
                                            }
                                            else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                                pre = "0";

                                        }


                                    }
                                    else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                    {
                                        pre = "0";
                                    }
                                }
                                else if (att2[Array.IndexOf(attname, "thal")] == "6")
                                {
                                    if (att2[Array.IndexOf(attname, "slope")] == "1" || att2[Array.IndexOf(attname, "slope")] == "2" || att2[Array.IndexOf(attname, "slope")] == "3")
                                        pre = "0";
                                }
                                else if (att2[Array.IndexOf(attname, "thal")] == "7")
                                {
                                    if (att2[Array.IndexOf(attname, "exang")] == "0")
                                    {
                                        if (att2[Array.IndexOf(attname, "cp")] == "1" || att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3")
                                        {
                                            pre = "0";
                                        }
                                        else if (att2[Array.IndexOf(attname, "cp")] == "4")
                                            pre = "1";

                                    }
                                    else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                        pre = "1";
                                }


                            }
                        }
                        else if (att2[Array.IndexOf(attname, "ca")] == "1")
                        {
                            if (att2[Array.IndexOf(attname, "cp")] == "1")
                            {
                                if (att2[Array.IndexOf(attname, "exang")] == "0")
                                    pre = "0";
                                else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                    pre = "0";
                            }
                            else if (att2[Array.IndexOf(attname, "cp")] == "2")
                            {
                                if (att2[Array.IndexOf(attname, "sex")] == "0")
                                {
                                    pre = "0";
                                }
                                else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                {
                                    if (att2[Array.IndexOf(attname, "thal")] == "3")
                                    {
                                        pre = "0";
                                    }
                                    else if (att2[Array.IndexOf(attname, "thal")] == "6")
                                    {
                                        pre = "0";
                                    }
                                    else if (att2[Array.IndexOf(attname, "thal")] == "7")
                                    {
                                        pre = "1";
                                    }
                                }
                            }
                            else if (att2[Array.IndexOf(attname, "cp")] == "3")
                            {
                                if (att2[Array.IndexOf(attname, "thal")] == "3")
                                    pre = "0";
                                else if (att2[Array.IndexOf(attname, "thal")] == "6")
                                {
                                    pre = "1";
                                }
                                else if (att2[Array.IndexOf(attname, "thal")] == "7")
                                {
                                    if (att2[Array.IndexOf(attname, "slope")] == "1")
                                        pre = "0";
                                    else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                        pre = "1";
                                    else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                        pre = "0";
                                }
                            }
                            else if (att2[Array.IndexOf(attname, "cp")] == "4")
                            {
                                if (att2[Array.IndexOf(attname, "sex")] == "0")
                                {
                                    if (att2[Array.IndexOf(attname, "slope")] == "1")
                                        pre = "0";
                                    else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                        pre = "1";
                                    else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                        pre = "0";

                                }
                                else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                {
                                    if (att2[Array.IndexOf(attname, "thal")] == "3")
                                        pre = "1";
                                    else if (att2[Array.IndexOf(attname, "thal")] == "6")
                                        pre = "1";
                                    else if (att2[Array.IndexOf(attname, "thal")] == "7")
                                    {
                                        if (att2[Array.IndexOf(attname, "slope")] == "1")
                                            pre = "1";
                                        else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                        {
                                            if (att2[Array.IndexOf(attname, "exang")] == "0")
                                                pre = "1";
                                            else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                            {
                                                if (att2[Array.IndexOf(attname, "oldpeak")] == "0")
                                                    pre = "1";
                                                else if (att2[Array.IndexOf(attname, "oldpeak")] == "1")
                                                    pre = "1";
                                            }
                                        }
                                        else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                            pre = "3";
                                    }

                                }
                            }
                        }
                        else if (att2[Array.IndexOf(attname, "ca")] == "2")
                        {
                            if (att2[Array.IndexOf(attname, "thal")] == "3")
                            {
                                if (att2[Array.IndexOf(attname, "cp")] == "1")
                                {
                                    if (att2[Array.IndexOf(attname, "sex")] == "0")
                                        pre = "0";
                                    else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                    {
                                        if (att2[Array.IndexOf(attname, "slope")] == "1")
                                            pre = "0";
                                        else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                            pre = "1";
                                        else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                            pre = "0";

                                    }
                                }
                                else if (att2[Array.IndexOf(attname, "cp")] == "2")
                                    pre = "0";
                                else if (att2[Array.IndexOf(attname, "cp")] == "3")
                                    pre = "0";
                                else if (att2[Array.IndexOf(attname, "cp")] == "4")
                                {
                                    if (att2[Array.IndexOf(attname, "sex")] == "0")
                                    {
                                        if (att2[Array.IndexOf(attname, "exang")] == "0")
                                        {
                                            if (att2[Array.IndexOf(attname, "oldpeak")] == "0")
                                                pre = "0";
                                            else if (att2[Array.IndexOf(attname, "oldpeak")] == "1")
                                            {
                                                if (att2[Array.IndexOf(attname, "slope")] == "1")
                                                    pre = "0";
                                                else if (att2[Array.IndexOf(attname, "slope")] == "2")
                                                    pre = "0";
                                                else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                                    pre = "1";

                                            }
                                        }
                                        else if (att2[Array.IndexOf(attname, "exang")] == "1")
                                            pre = "1";
                                    }
                                    else if (att2[Array.IndexOf(attname, "sex")] == "1")
                                        pre = "1";
                                }
                            }
                            else if (att2[Array.IndexOf(attname, "thal")] == "6")
                                pre = "1";
                            else if (att2[Array.IndexOf(attname, "thal")] == "7")
                                pre = "1";
                        }
                        else if (att2[Array.IndexOf(attname, "ca")] == "3")
                        {
                            if (att2[Array.IndexOf(attname, "slope")] == "1")
                            {
                                if (att2[Array.IndexOf(attname, "cp")] == "1" || att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3")
                                {
                                    pre = "0";
                                }
                                else if (att2[Array.IndexOf(attname, "cp")] == "4")
                                {
                                    if (att2[Array.IndexOf(attname, "oldpeak")] == "0")
                                    {
                                        if (att2[Array.IndexOf(attname, "thal")] == "3")
                                            pre = "1";
                                        else if (att2[Array.IndexOf(attname, "thal")] == "6")
                                            pre = "0";
                                        else if (att2[Array.IndexOf(attname, "thal")] == "7")
                                            pre = "0";

                                    }
                                    else if (att2[Array.IndexOf(attname, "oldpeak")] == "1")
                                        pre = "1";
                                }

                            }
                            else if (att2[Array.IndexOf(attname, "slope")] == "2")
                            {
                                if (att2[Array.IndexOf(attname, "cp")] == "1")
                                    pre = "0";
                                else if (att2[Array.IndexOf(attname, "cp")] == "2" || att2[Array.IndexOf(attname, "cp")] == "3" || att2[Array.IndexOf(attname, "cp")] == "4")
                                {
                                    pre = "1";
                                }
                            }
                            else if (att2[Array.IndexOf(attname, "slope")] == "3")
                                pre = "1";
                        }

                        double thry = (0.0000006706 * 6555), thrn = (0.00000296 * 6555);
                       // double thry = 100 / z1, thrn = 100 / z1;
                        //Console.WriteLine(tprody + "," + tprodn);
                        //Console.ReadLine();
                        //fileper.WriteLine(tprody + "," + tprodn + "," + thry + "," + thrn + "," + pre);
                       // Console.WriteLine(z1);
                        if (tprody > tprodn && pre == "1")
                        {
                            file2.WriteLine(line + "," + "1");
                          //  fileper.WriteLine(tprody + "," + tprodn + "," + thry + "," + thrn + "," + pre);
                        }
                        else if (tprody <= tprodn && pre == "0")
                        {
                            file2.WriteLine(line + "," + "0");
                            //fileper.WriteLine(tprody + "," + tprodn + "," + thry + "," + thrn + "," + pre);
                        }
                        else
                        {
                            if (tprody > tprodn)
                            {
                                if (tprody >= thry)
                                    file2.WriteLine(line + "," + "1");
                                else
                                    file2.WriteLine(line + "," + "0");
                            }
                            else
                            {
                                if (tprodn >= thrn)
                                    file2.WriteLine(line + "," + "0");
                                else
                                    file2.WriteLine(line + "," + "1");
                            }
                        }
                    }
                    file.Close();
                    file1.Close();
                    file2.Close();
                    //fileper.Close();


                    System.IO.StreamReader file10 =
                              new System.IO.StreamReader("C:\\Users\\SHUBHANKAR\\Desktop\\New folder (2)\\test_data\\predicted_data_naivebayes_DT_combined_temp.csv");
                    string[] att3 = new string[9];
                    double count4 = 0.0, count5 = 0.0;
                    String line1;
                    while ((line1 = file10.ReadLine()) != null)
                    {
                        att3 = line1.Split(',');
                        count4++;
                        //Console.WriteLine(att3[7] + "==>" + att3[8]);
                        if (att3[7] == att3[8])
                            count5++;
                    }
                    Console.WriteLine("Percentage of correct prediction=" + ((count5 / 301) * 100) + "%");

                    file10.Close();
                
                    Console.WriteLine("working");
                    Console.ReadLine();
                    
            }
                

        }
    }
}
